#TDA Marketplace
An online marketplace by TDA where the community can post an ads selling their merchandise.
--
##API Endpoints:
- api/v1/login/
    + POST: Login.
- api/v1/user/
    - **USEROBJECT**: `{ avatar: str, password: str, email: str, name: str, address: str, state: str, country: int, latitude: str, longitude: str }`
    + POST: Registers a user.
        + Input: USEROBJECT. Mandatory fields: `email` & `password`
        + Response: OK 200, or Bad Request 400 with `{ errors: USEROBJECT }` object
    + PUT: Updates user data.
        + Input: USEROBJECT
        + Response: OK 200, or Bad Request 400 with `{ errors: USEROBJECT` } object
    + DELETE: Deletes current user's data
    + GET: Get current user's data.
        + Response: USEROBJECT
- api/v1/create/ads

##User Types:
This section needs updating.
- Advertiser
    + Post an ad
- Admin
    + Review & Approve an ad request
    + Manage users
    + Ads dashboard
###Ad Category
- Vehicles
- Sports & Outdoors
- Home Appliances
- Electronics
- Hobbies
- Fashion & Accessories
- Entertainment & Media
- Others

###Ad Types
- Rental
- For Sale

###Create an ad
1. A registered user click on Create Ad
2. Fills in the form
    - Title
    - Description
    - Category
    - Type
    - Photos
    - Price
    - Contact Number (Since registered, taken from the user's data)