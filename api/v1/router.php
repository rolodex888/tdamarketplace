<?php
	/**
	loading library
    **/
    require("Horus.php");
    require("rb.php");

    /**
    instantiate app object
    **/
    $app = new \Horus\App();

    /**
    uri callback 
    **/
    //root
    $app->on('/', function($req, $res){
    	ob_start();
		include 'api.html';
		$out = ob_get_contents();
		ob_end_clean(); 
        $res->send($out);
        $res->end();
    });
 
 	//access
    $app->on('POST /login', function($req, $res){
        $res->end('login controller"');
    });
    $app->on('GET /logout', function($req, $res){
        $res->end('logout controller"');
    });

    //user 
    $app->on('GET /user', function($req, $res){
        $res->end("user controller");
    });
    $app->on('PUT /user', function($req, $res){
        $res->end("user controller");
    });

    // ads controller
    $app->on('/ads', function($req, $res){
        $res->end("create controller");
    });

    // wildcards ?
    $app->on('/test/*', function($req, $res, $path){
        $res->end("you are in 'test/{$path}'");
    });

    // group of routes ?
    $app->group("/group", function($req, $res, $app){
        // group/page-1
        $app->on('/page-1', function(){});
        // you can create nested groups too .
    });

    // vhosts ?
    $app->vhost("user-*.my.com", function($req, $res, $app, $uid){
        // you can apply routes here too
    });

    // lets put this at the end of our routes
    // so if "PHP" didn't find the right route
    // it will display '404'
    $app->on('/*', function($req, $res){
        $res->end("404 not found");
    });